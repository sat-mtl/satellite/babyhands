# Babyhands



## Introduction

Babyhands is a node.js middleware that allows a bi-directional socket.io communication between Satellite and any software using socket.io 2, such as Touchdesigner. <br>
Babyhands can also send and receive OSC data.

## Installation

Create a .env file in the folder where you cloned the repository with the follwing values: 
```
HTTP_WEB_PORT = 8558 
SOCKET_IO_HOST_URI = https://api.satellite.sat.qc.ca # or https://api.satlit.sat.qc.ca if testing with satlit
OSC_CLIENT_IP = 127.0.0.1
OSC_RECEIVE_PORT = 7403
OSC_SEND_PORT = 7404
HUB_ID = #id of the room you are sending or receiving data from, example: gg6CydS
SOCKET_IO_SERVER_PORT = 3000 
```

Install NodeJS if you haven't already. We recommend version 12 or above. <br>
Run the following command in the folder where you cloned the repository:

```
npm install
node index.js
```

## Socket.io communication

### Sending data

The hubsRoomIOURISender constant subscribes to a socket.io 4.1.1 on satellite on the proper channel with a socket.io 4.1.1 client. <br>
In order to send data to a room, the message must take the following structure: 
```
let msgObj = { 
    'hubsID':process.env.HUB_ID, 
    "msgType":yourEvent, 
    "msg": {
        "msgType":yourEvent, 
        "data":yourData
    } 
};
```
Since the data is received from a socket.io 2 server, the code can be contained in the following event: 
```
socketIO2Server.on('connection', (client)=> { 
    client.on("yourEvent", (msg) => { 
        let msgObj = { 
            'hubsID':process.env.HUB_ID, 
            "msgType":yourEvent, 
            "msg": {
                "msgType":yourEvent, 
                "data":msg
            } 
        };
        hubRoomSocket.emit("yourEvent", msgObj) # You can send a different event name to the room
    });
});
```
### Receiving data
The hubsRoomIOURISender constant listens to all events sent from a room. <br>
To listen to a following event, you can add the following code:
```
hubRoomSocketReceiver.on("yourEvent", (msg) => { 
    socketIO2Server.emit("yourEvent", msg); # Send the data to your socket.io 2 server. The event name can be different than the one you receive from the room
});
```
## Sending OSC data
The OSCServer listens to the 'message' event, deconstructs the received OSC message and sends it back to the room: 
```
OSCServer.on('message', (msg) => {    
    if(msg[0].charAt(0) === '/') {
        const ap = `${msg[0]}`; #Corresponds to the name of the event
        msg.shift();
        const message = {
            date: new Date().toUTCString(),
            address: ap,
            tag: msg,
        }
            let msgObj = { 
            'hubsID':process.env.HUB_ID, 
            "msgType":ap, # You can send a different event name
            "msg": {
                    "msgType":ap, 
                    "data":msg
                } 
        };
        setTimeout(()=> {
            hubRoomSocketSender.emit("yourEvent", msgObj)
        }, 100);

    } else {
        console.log('error, outgoing OSC Message must lead with an addressPattern\n\ni.e. /bioData')
    }
});
```