const ioclient = require('socket.io-client');
const { Client, Server } = require('node-osc');

require('dotenv').config();

const socketOptions = {
    withCredentials: true,
    extraHeaders: { "hubs-header": "satmontreal1234" },
    transports: ['websocket']
};

const hubsRoomIOURIReceiver = `${process.env.SOCKET_IO_HOST_URI}/data-satlit`;
const hubsRoomIOURISender = `${process.env.SOCKET_IO_HOST_URI}/hubsRooms-satlit`;

let hubRoomSocketReceiver;
let hubRoomSocketSender;

const socketIO2Server = require('socket.io')();
socketIO2Server.on('connection', (client)=> { });

hubRoomSocketReceiver = ioclient(hubsRoomIOURIReceiver, socketOptions);
hubRoomSocketReceiver.emit("register", {"channel":process.env.HUB_ID})
hubRoomSocketReceiver.on("connect", ()=> { console.log(`${connPrefix} ${hubsRoomIOURIReceiver}`); });
hubRoomSocketReceiver.on("error", (msg) => { console.log(msg); });
hubRoomSocketReceiver.on("connect_error", (msg) => { console.log(msg); });

hubRoomSocketSender = ioclient(hubsRoomIOURISender, socketOptions);
hubRoomSocketSender.on("connect", ()=> { console.log(`${connPrefix} ${hubsRoomIOURISender}`); });
hubRoomSocketSender.on("error", (msg) => { console.log(msg); });
hubRoomSocketSender.on("connect_error", (msg) => { console.log(msg); });



socketIO2Server.listen(process.env.SOCKET_IO_SERVER_PORT);

const OSCClient = new Client(process.env.OSC_CLIENT_IP, process.env.OSC_SEND_PORT); 
const OSCServer = new Server(process.env.OSC_RECEIVE_PORT, '0.0.0.0');

OSCServer.on('listening', () => { })
OSCServer.on('message', (msg) => {
    
    if(msg[0].charAt(0) === '/') {
        const ap = `/${msg[0]}`;
        msg.shift();
        const message = {
            date: new Date().toUTCString(),
            address: ap,
            tag: msg,
        }
        console.log(msg);
    } else {
        console.log('error, outgoing OSC Message must lead with an addressPattern\n\ni.e. /bioData')
    }
});



emitSocketSignalToOSC=(address, tag)=> {
    // composer addreess + tag     
    OSCClient.send(address, tag, (err) => {
        if (err) console.error(err);
    });
}

// http web server with routes, maybe we want to do stuff with that later on
const fastify = require('fastify')({ logger: true })
fastify.get('/', async (request, reply) => {
    return { status: 'BabyHands is running.' }
})

const start = async() => {
    try {
      await fastify.listen(process.env.HTTP_WEB_PORT)
    } catch (err) {
      fastify.log.error(err)
      process.exit(1)
    }
}
start();